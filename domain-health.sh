#!/bin/bash

which timeout 2>&1|grep -q /timeout || timeout() { ( $( echo "$@"|cut -d" " -f2-)  &  sleep $1; kill $!) ; } ;

which fuser |grep -q fuser || {
    which apk >/dev/null && apk add psmisc ;
    which apt-get >/dev/null && { apt-get update ;apt-get install psmisc ; } ;
    echo -n ; } ;

[[ -z "$DEBUGME" ]] && DEBUGME=false
locked=no
scriptname="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")"

lockfile=/tmp/$scriptname.lock
#now check if the pid from the lockfile has our scriptame in proc cmdline

test -f $lockfile && echo -n lock:$(cat $lockfile) && grep "$scriptname" /proc/$(cat $lockfile)/cmdline 2>/dev/null && locked=yes

if [ "$locked" = "yes" ] ;then

echo "I SEEM TO BE RUNNING with  pid $(cat $lockfile) as stated in lockfile $lockfile , delete it if certain" >&2

else

echo -n locking; echo "$$" > $lockfile

fi

#todo : git reset head ,  git directory
test -f ~/.domainhealth.conf || (echo "NO CONFIG";exit 3 )
. ~/.domainhealth.conf


_domain_exists() { [[ -z "$1" ]] && return 0; ( ( nslookup "$1" 2>&1 || host "$1" 2>&1  || dig "$1" 2>&1 ) ) |grep -q -e nxdomain -e NXDOMAIN && return 1 || return 0 ; } ;

## expects host:port
_ssl_host_enddate_days() {
    target=$1
    [[ "$DEBUGSSL" = "true" ]] && { echo >&2 ;echo timeout 10 openssl s_client '\-connect' "$target" >&2 ;echo ;};
    end=$(echo | timeout 10 openssl s_client -connect "$target" 2>/dev/null |openssl x509 -enddate -noout  | sed -e 's#notAfter=##'|awk '{ printf "%04d-%02d-%02d\n", $4,(index("JanFebMarAprMayJunJulAugSepOctNovDec",$1)+2)/3, $2}');
    #echo $end
    date_diff=$(( ($(date -d "$end" +%s) - $(LC_ALL=C date -d "$(LC_ALL=C date +%Y-%m-%d)" +%s) )/(60*60*24) ));printf '%s: %s' "$date_diff" "$target" ; } ;

_vhost_extract_docker() {
    ##gen list
    containers=$(docker ps --format '{{.Names}}' |grep -v -e nginx -e portainer|grep "\." ) ;
    #docker exec $i printenv SSH_PORT
    webcontainers=$(for i in $containers;do imagetype=$(timeout 15 docker inspect --format '{{.Config.Image}}' $i) ; echo $imagetype |grep -q -e ^mariadb -e _cron$ -e memcached -e _database -e piwik-cron -e nginx-gen -e nginx-letsencrypt -e nginx-proxy && (echo will not proc $i"     "  >&2)|| ( echo -en "$i ";) ; done)
    (for i in $webcontainers;do
        imagetype=$(timeout 15 docker inspect --format '{{.Config.Image}}' $i)  ;
        VIRTHOST=$(docker exec $i printenv VIRTUAL_HOST)
        [[ -z "$VIRTHOST" ]] || {
        REDIRSCHEME=$(docker exec $i printenv SERVER_REDIRECT_SCHEME)
        [[ -z $REDIRSCHEME ]] && REDIRSCHEME=https
        echo $imagetype | grep -q -e nginx-redirect -e redirect && {
          echo "R@"$i"@"$(docker exec $i printenv VIRTUAL_HOST)"@@"${REDIRSCHEME}"://"$(docker exec $i printenv SERVER_REDIRECT)$(docker exec $i printenv SERVER_REDIRECT_PATH) ; } ||  {
          echo "H@"$i"@"$(docker exec $i printenv VIRTUAL_HOST)"@"$(docker port $i|grep ^22|wc -l|grep -q ^0$ ||timeout 15 docker inspect --format '22/tcp:{{ (index (index .NetworkSettings.Ports "22/tcp") 0).HostPort }}' $i | grep "22/tcp" |cut -d":" -f2)"@" ; } ;
        echo -n ; } ;
    done > /tmp/vhostconf.domainlist)
    #cat /tmp/vhostconf.domainlist
echo ; } ;

_vhost_extract_apache() {
    containers=$(docker ps --format '{{.Names}}' |grep -v -e nginx -e portainer );
    apachecfg="$(apache2ctl -S 2>/dev/null || apachectl -S 2>/dev/null)";
    configlines=$(echo "$apachecfg"|grep -e namevhost -e "alias "|sed 's/.\+namevhost /@@/g;s/.\+ alias /:/g'|tr -d '\n'|sed 's/$/@@/g'|sed 's/@@/\n/g');
    echo "$configlines"|grep -v ^$|while read cur_config; do
        host=$(echo $cur_config|cut -d" " -f1);
        conffile=$(echo $cur_config|cut -d"(" -f2 |cut -d":" -f1);
        vhosts=$(echo $cur_config | cut -d")" -f2-|grep -v ^$|cut -d":" -f2-|sed 's/:/,/g' )
        vhostfield=$(echo $host","$vhosts|sed 's/,$//g')
        redir=$((curl -sw "\n\n%{redirect_url}" "https://${host}" | tail -n 1|grep -q http ) && echo "R" || echo "H" )
        ssh_port=$(echo "$containers"|grep -q "^"$host"$" && echo -n $(docker port $host|grep ^22|wc -l|grep -q ^0$ ||timeout 15 docker inspect --format '22/tcp:{{ (index (index .NetworkSettings.Ports "22/tcp") 0).HostPort }}' $host | grep "22/tcp" |cut -d":" -f2))
        echo $redir"@"$host"@"$vhostfield"@"$ssh_port
    done |  awk '!x[$0]++' > /tmp/vhostconf.domainlist
echo -n ; } ;

_vhost_extract_nginx() {
    which docker >/dev/null && containers=$(docker ps --format '{{.Names}}' |grep -v -e nginx -e portainer );
    cat /etc/nginx/sites-enabled/*|sed 's/^\( \|\t\)\+#.\#//g;s/#.\+//g'|grep -v ^$|sed 's/server /@@server /g;s/^/→→/g'|tr -d '\n'|sed 's/@@/\n/g'|grep "listen 443" |sed 's/→→ /\n/g'|grep -e "server " -e server_name |sed 's/\;.\+//g'|sed 's/server /@@server /g'|tr -d '\n'|sed 's/@@/\n/g'|grep -v ^$|sed 's/^server {//g;s/^\( \|\t\)\+//g;s/server_name//;s/;//g'|while read vhosts ;do
        vhostfield=$(echo $vhosts|sed 's/ \+/ /g;s/ /,/g');
        host=$(echo "$vhosts"|sed 's/ /\n/g'|grep -v "*"|head -n1);
        redir=$((curl -sw "\n\n%{redirect_url}" "https://${host}" | tail -n 1|grep -q http ) && echo "R" || echo "H" )
        which docker >/dev/null && ssh_port=$(echo "$containers"|grep -q "^"$host"$" && echo -n $(docker port $host|grep ^22|wc -l|grep -q ^0$ ||timeout 15 docker inspect --format '22/tcp:{{ (index (index .NetworkSettings.Ports "22/tcp") 0).HostPort }}' $host | grep "22/tcp" |cut -d":" -f2))
        echo $redir"@"$host"@"$vhostfield"@"$ssh_port
    done  |  awk '!x[$0]++' > /tmp/vhostconf.domainlist
echo ; }

_websrv_health_client() {
[ -z "$CLIENT_GIT_REPO" ] && ( echo "no target repo" ; exit 3 )
echo
echo -n "::CLIENT..."
test -e /dev/shm/.domainhealth-stubs|| mkdir  /dev/shm/.domainhealth-stubs
find /dev/shm/.domainhealth-stubs/ -name ".json_*" -delete
echo -n "::PULL or CLEAN::"
###first the master folder
test -d ${HOME}/.domain-health-list/.git && ( cd ${HOME}/.domain-health-list/ ; git pull --recurse-submodules ) || (rm -rf ${HOME}/.domain-health-list/; echo -n " |::CLONE: "$CLIENT_GIT_REPO" "$repository  ; pwd | tr -d '\n' ; git clone $CLIENT_GIT_REPO ${HOME}/.domain-health-list 2>&1; cd ${HOME}/.domain-health-list; git pull --recurse-submodules 2>&1  ) |sed 's/$/|/g' |tr -d '\n'
##then single folders
test -d ${HOME}/.domain-health-lists/ || ( rm -rf ${HOME}/.domain-health-lists 2>&1 ; mkdir ${HOME}/.domain-health-lists 2>&1  ) |sed 's/$/|/g' | tr -d '\n'
##echo
cd ${HOME}/.domain-health-lists &&  ( cd ${HOME}/.domain-health-lists ;cat ${HOME}/.domain-health-list/repolist  | while read repository ;do echo -n " |::CLONE: "$CLIENT_GIT_REPO" "$repository  ; pwd ; (git clone "$repository" 2>&1 |grep -v "already exists" )    ;done) |sed 's/$/|/g' | tr -d '\n'

cd /tmp/;
[[ "$DEBUGME" = "true" ]] && echo $(date +%F-%T) debug  start statcode >&2
### if force-push happened , pull from beginning
(  for fold in ${HOME}/.domain-health-lists/domainlist-*;do cd $fold;git update-ref -d HEAD 2>&1 ;git rm -fr . 2>&1 ;git pull 2>&1 ;done ) |sed 's/$/|/g' |tr -d '\n'
# for fold in ${HOME}/.domain-health-lists/domainlist-*;do cd $fold;git reset --hard origin/master;git pull ;done
# for fold in ${HOME}/.domain-health-lists/domainlist-*;do cd $fold;git pull ;done
# cd ${HOME}/.domain-health-lists/ ; git pull --recurse-submodules
#!! 500 ( internal server err) → contao no startpoint , cache failures, php code errors etc.
#!! 503 onlyoffice cant write
#!! 503 Service Temporarily Unavailable
#test status

statusgetter() {
[[ "$DEBUGME" = "true" ]] && echo >&2
find ${HOME}/.domain-health-lists/ -name domainlist|while read listfile;do cat "$listfile";done |sort -u |grep -v -e "^H@@@$" -e "^R@@@$"|awk '!x[$0]++' |while read a ;do (
target="";
type=${a/%@*/};
url=$(echo $a|cut -d" " -f1|cut -d@ -f2|cut -d"," -f1)
_domain_exists $url || url=$(echo $a|cut -d" " -f1|cut -d@ -f3|cut -d"," -f1)
##url is first vhost if name of container is not a domain

[[ "$DEBUGME" = "true" ]] && echo $(date +%F-%T) debug $url statusgetter "$a" >&2
    if [ "$type" == "H" ];then  url=$(echo $a|cut -d" " -f1|cut -d@ -f3|cut -d"," -f1);
                                http_stat=$(curl -ksw '%{http_code}' https://$url -o /dev/null 2>&1);fi
    if [ "$type" == "R" ];then  url=$(echo $a|cut -d" " -f1|cut -d@ -f3|cut -d"," -f1);
                                http_stat=$(curl -ksw '%{http_code}' https://$url -o /dev/null 2>&1); target=$(curl -k -I -L -s -S -w %{url_effective} -o /dev/null $url) ; fi; echo $http_stat"@"$a"@"$target > /dev/shm/.domainhealth-stubs/.status_$(echo $url|base64 |tr -dc '[:alnum:]') ) &
        ## throttle if we have more load than cpus , the sleep interval in seconds is determinted  like = 0.23s + loadavg minus no-of-cores
        if [  "$(cut -d. -f1 /proc/loadavg)" -ge "$(nproc)" ];then
	                  sleep 0.22 ;
                  sleeptime=$((  $(cut -d. -f1 /proc/loadavg)  - $(nproc) )) ;
                  [[ "$DEBUGME" = "true" ]] && echo SLEEP $sleeptime >&2
                  sleep ${sleeptime} ;
        fi

        ## throttle if we have more load than cpus , the sleep interval in seconds is determinted  like =  loadavg (INT) seconds
        if [  "$(cut -d. -f1 /proc/loadavg)" -ge "$(($(nproc)+1))" ];then
                  sleeptime=$((  $(cut -d. -f1 /proc/loadavg) )) ;
                  [[ "$DEBUGME" = "true" ]] && echo SLEEP $sleeptime >&2
                  sleep ${sleeptime} ;
        fi
done
wait
sleep 2;
find /dev/shm/.domainhealth-stubs/ -name ".status_*" -mtime +1 -delete
cat /dev/shm/.domainhealth-stubs/.status_*
}


#statusobject="$(statusgetter)"
statusgetter>/tmp/.domainhealth.csv

#echo "$statusobject" > /tmp/domainhealthstatusobj.dump
#statuslength=$(echo "$statusobject"|wc -l)
statuslength=$(cat /tmp/.domainhealth.csv|wc -l)



## begin json gen
(
[[ "$DEBUGME" = "true" ]] && echo >&2
    ##w2ui json init
    count=1;
    [[ "$DEBUGME" = "true" ]] && echo debug: entry gen >&2
    test -d /tmp/.domainhealth-www || mkdir -p /tmp/.domainhealth-www
    #echo "$statusobject" |while read entry ;do
    bckIFS=$IFS
    IFS=$'\n'
    #cat  /tmp/.domainhealth.csv|while read entry ;do
    for entry in $(cat  /tmp/.domainhealth.csv);do
      {
      nxdomain=no;
      instancename=$(echo $entry|cut -d"@" -f 3)
      targetdomain=${instancename}
      _domain_exists "${targetdomain}" || {

      for myvhost in $(echo "${entry}"|cut -d@ -f 4|sed 's/,/\n/g') ;do
      _domain_exists ${myvhost} && {
                                     [[ "$DEBUGME" = "true" ]] && echo domaincorected: $targetdomain TO $myvhost>&2
                                    targetdomain=${myvhost}; break ; } ;
      ## correct targetdomain to first vhost url if nonexistent
      done ; } ;
      _domain_exists "${targetdomain}" ||  nxdomain=yes
      (host -t a "${targetdomain}";host -t aaaa "${targetdomain}"  ;host -t cname "${targetdomain}")|grep "has address" -q || nxdomain=yes
      echo ${targetdomain}|grep -e '.lan$' -e '.local$' && ndomain=yes
      #  echo $targetdomain $nslookup >&2
      [[ "$DEBUGME" = "true" ]] && echo "NXDOMAIN: $nxdomain" >&2
      if [ "$nxdomain" = "no" ]; then
        ssldays=$(_ssl_host_enddate_days "${targetdomain}:443" |cut -d":" -f1|tr -d '\n'); else ssldays="0" ;
      fi
      sleep 0.42
      ##fix empty host ( fill from first vhost)
      if [ $(echo "${entry}"|grep ^[0-9]*@[RH]@@) ];then
          firstvhost=$(echo "${entry}"|cut -d@ -f 4|cut -d, -f1);
          entry=$(echo  "${entry}"|cut -d@ -f1-2;echo -n "@"${firstvhost}"@";echo -n "${entry}"|cut -d@ -f4-) ;
          entry=${entry/[[:space:]]/};
      fi
     [[ "$DEBUGME" = "true" ]] && echo $(date +%F-%T) debug " ${count} / $statuslength STATUSOBJECT IN:" $entry 1>&2

      #echo "$entry"|awk -F @ '{print "{ \"recid\": \""${count}"\", \"type\": \""$2"\", \"status\": \""$1"\", \"vhost\": \""$3"\", \"ssh\": \""$5"\", \"ssldays\": \""'${ssldays}'"\", \"redirect\": \""$6"\", \"alias\": \""$4"\" }"}'|tr -d '\n'
      #echo "$entry"|awk -F @ '{print "{ \"recid\": \""${count}"\",

     redirect=$(echo "$entry"|cut -d"@" -f6)
######ssldays=$(echo "$entry"|cut -d"@" -f)
       status=$(echo "$entry"|cut -d"@" -f1)
        alias=$(echo "$entry"|cut -d"@" -f4)
        vhost=$(echo "$entry"|cut -d"@" -f3)
         type=$(echo "$entry"|cut -d"@" -f2)
          ssh=$(echo "$entry"|cut -d"@" -f5)
      finalentry='{"recid":"'${count}'","type":"'${type}'","status":"'${status}'","vhost":"'${vhost}'","ssh":"'${ssh}'","ssldays":"'${ssldays}'","redirect":"'${redirect}'","alias":"'${alias}'"}'
      echo -n "$finalentry" > /dev/shm/.domainhealth-stubs/.json_$(printf %0.10d ${count})
      [ ${count}  -ne $statuslength ] && echo "," >> /dev/shm/.domainhealth-stubs/.json_$(printf %0.10d ${count})  ## no comma on last line
      [[ "$DEBUGME" = "true" ]] && { echo "$finalentry" >&2 ; } ;
        echo -n ; } &
        let count++;
        ## throttle if we have more load than cpus , the sleep interval in seconds is determinted  like = 0.23s + loadavg modulo no-of-cores
        if [  "$(cut -d. -f1 /proc/loadavg)" -ge "$(nproc)" ];then
                  sleep 0.42 ;
                  sleeptime=$((  $(cut -d. -f1 /proc/loadavg)  - $(nproc) )) ;
                  [[ "$DEBUGME" = "true" ]] && echo SLEEP $sleeptime >&2
                  sleep ${sleeptime} ;
        fi
    done
    wait
    echo '{';echo '"total": '${statuslength}",";echo '"records": [';
    cat /dev/shm/.domainhealth-stubs/.json_*
    echo -e "\t"']';
         echo '}';
sleep 0.42;
) | tr -cd '0-9a-zA-Z"\-.:;{}@_,/[]' |tee  /tmp/.domainhealth-www/new_domainlist.json |nl >&2 ;

wait
find /dev/shm/.domainhealth-stubs/ -name ".status_*" -mtime +1 -delete
[[ "$DEBUGME" = "true" ]] && echo $(date +%F-%T) debug fix json >&2
## replace comma on last line end
cat /tmp/.domainhealth-www/new_domainlist.json |jq >/dev/null  || ( cat /tmp/.domainhealth-www/new_domainlist.json |tr -d '\n'|sed 's/},\t]}/}\t]}/g'  > /tmp/.domainhealth-www/new_domainlist.json.tmp ;
mv /tmp/.domainhealth-www/new_domainlist.json.tmp /tmp/.domainhealth-www/new_domainlist.json   )

#####DROP FILE IF NO VALID JSON
cat /tmp/.domainhealth-www/new_domainlist.json |tr -d '\n'|grep -q '}$' &&  mv /tmp/.domainhealth-www/new_domainlist.json  /tmp/.domainhealth-www/domainlist.json|| rm /tmp/.domainhealth-www/new_domainlist.json   ;

### picoINFLUX INSERT POINT####
## since out json object is written now , we immediatly report to influx
#test -f $HOME/.picoinflux.conf && ( _transmit_influx )
test -f $HOME/.picoinflux.conf && (

timestamp_nanos() { if [[ $(date +%s%N |wc -c) -eq 20  ]]; then date -u +%s%N;else expr $(date -u +%s) "*" 1000 "*" 1000 "*" 1000 ; fi ; } ;
# TARGET FORMAT  : load_shortterm,host=SampleClient value=0.67
# TARGET_FORMAT_T: load,shortterm,host=SampleClient value=
# CREATE ~/.picoinflux.conf with first line user:pass second line url (e.g. https://influxserver.net:8086/write?db=collectd
# ADDITIONALLY set custom hostname in /etc/picoinfluxid

hostname=$(cat /etc/picoinfluxid 2>/dev/null || (which hostname >/dev/null && hostname || (which uci >/dev/null && uci show |grep ^system|grep hostname=|cut -d"'" -f2 ))) 2>/dev/null
######### main  ####################

[[ "$DEBUGME" = "true" ]] && echo $(date +%F-%T) debug json2influx >&2

### json2influx
test -f /tmp/.domainhealth-www/domainlist.json && (
        ### get curl status codes
          cat /tmp/.domainhealth-www/domainlist.json |tr -d '\n'|grep -q '}$' && cat /tmp/.domainhealth-www/domainlist.json | jq -c '.records[]' | jq -c '[.vhost,.status]' | while read res;do \
                                    echo statuscode_vhost,target=$(echo $res|cut -d'"' -f2)=$(echo $res|cut -d'"' -f4) & done  2>/dev/null |grep -v =$| while read linein;do
                                                  echo "${linein}" | sed 's/\(.*\)=/\1,host='"$hostname"' value=/'|sed 's/$/ '$(timestamp_nanos)'/g' ;done|grep -v "=," |grep "=" >> ~/.influxdata.vhost  ;
        #get ssl valid days
           cat /tmp/.domainhealth-www/domainlist.json |tr -d '\n'|grep -q '}$' && cat /tmp/.domainhealth-www/domainlist.json | jq -c '.records[]' | jq -c '[.vhost,.ssldays]' | while read res;do \
                                    echo ssldays_vhost,target=$(echo $res|cut -d'"' -f2)=$(echo $res|cut -d'"' -f4) & done  2>/dev/null |grep -v =$| while read linein;do
                                                  echo "${linein}" | sed 's/\(.*\)=/\1,host='"$hostname"' value=/'|sed 's/$/ '$(timestamp_nanos)'/g' ;done|grep -v "=," |grep "=" >> ~/.influxdata.vhost  ;
          sleep 0.4 )

## sed 's/=/,host='"$hostname"' value=/g'
##TRANSMISSION STAGE::
##check config presence of secondary host and replicate
grep -q "^SECONDARY=true" $HOME/.picoinflux.conf && (
	( ( test -f $HOME/.influxdata.vhost && cat $HOME/.influxdata.vhost ; test -f $HOME/.influxdata.vhost.secondary && cat $HOME/.influxdata.vhost.secondary ) | sort |uniq > $HOME/.influxdata.vhost.tmp ;
  mv $HOME/.influxdata.vhost.tmp $HOME/.influxdata.vhost.secondary )  ##



	grep -q "^TOKEN2=true" $HOME/.picoinflux.conf && ( (curl -s -k --header "Authorization: Token $(grep ^AUTH2= $HOME/.picoinflux.conf|cut -d= -f2-)" -i -XPOST "$(grep ^URL2 ~/.picoinflux.conf|cut -d= -f2-)" --data-binary @$HOME/.influxdata.vhost.secondary 2>&1 && rm $HOME/.influxdata.vhost.secondary 2>&1 ) >/tmp/picoinflux.secondary.log  )  || ( \
	(curl -s -k -u $(grep ^AUTH2= $HOME/.picoinflux.conf|cut -d= -f2-) -i -XPOST "$(grep ^URL2 $HOME/.picoinflux.conf|cut -d= -f2-|tr -d '\n')" --data-binary @$HOME/.influxdata.vhost.secondary 2>&1 && rm $HOME/.influxdata.vhost.secondary 2>&1 ) & ) >/tmp/picoinflux.vhost.secondary.log
	)


grep -q "TOKEN=true" ~/.picoinflux.conf && ( (curl -s -k --header "Authorization: Token $(head -n1 $HOME/.picoinflux.conf)" -i -XPOST "$(head -n2 ~/.picoinflux.conf|tail -n1)" --data-binary @$HOME/.influxdata.vhost 2>&1 && rm $HOME/.influxdata.vhost 2>&1 )  >/tmp/picoinflux.vhost.log )  || ( \
	(curl -s -k -u $(head -n1 $HOME/.picoinflux.conf) -i -XPOST "$(head -n2 $HOME/.picoinflux.conf|tail -n1)" --data-binary @$HOME/.influxdata.vhost 2>&1 && rm $HOME/.influxdata.vhost 2>&1 ) >/tmp/picoinflux.vhost.log  )

#(curl -s -k -u $(head -n1 ~/.picoinflux.conf) -i -XPOST "$(head -n2 ~/.picoinflux.conf|tail -n1)" --data-bary @$HOME/.influxdata.vhost 2>&1 && mv $HOME/.influxdata.vhost $HOME/.influxdata.sent 2>&1 ) >/tmp/picoinflux.vhost.log





##picoinflux.conf examples (first line pass/token,second line url URL , rest is ignored except secondary config)
##example V1
#user:buzzword
#https://corlysis.com:8086/write?db=mydatabase

##example V2
#KJAHSKDUHIUHIuh23ISUADHIUH2IUAWDHiojoijasd2asodijawoij12e_asdioj2ASOIDJ3==
#https://eu-central-1-1.aws.cloud2.influxdata.com/api/v2/write?org=deaf13beef12&bucket=sys&&precision=ns
#TOKEN=true

##  add the following lines for a backup/secondary write with user/pass auth:                                                                                                                                                                 # SECONDARY=true
# URL2=https://corlysis.com:8086/write?db=mydatabase
# AUTH2=user:buzzword                                                                                                                                                                                                                         # TOKEN2=false
#

##  add the following lines for a backup/secondary write with token (influx v2):
# SECONDARY=true
# URL2=https://eu-central-1-1.aws.cloud2.influxdata.com/api/v2/write?org=deaf13beef12&bucket=sys&&precision=ns
# AUTH2=KJAHSKDUHIUHIuh23ISUADHIUH2IUAWDHiojoijasd2asodijawoij12e_asdioj2ASOIDJ3==
# TOKEN2=true

#
 ## END INFLUX ###


)




 echo -n ; } ;

_host_extract() {
    [ -z "$HOST_GIT_REPO" ] && ( echo "no target repo" ; exit 3 )
    websrvid=$(ls -l /proc/$(fuser 443/tcp 2>/dev/null|awk '{print $1}')/exe)
    case "${websrvid}" in
       *docker-proxy )
                 #echo docker;
                 _vhost_extract_docker ;;
       *apache2 )
                 #echo apache2;
                  _vhost_extract_apache ;;
       *nginx )
                 #echo nginx;
                  _vhost_extract_nginx ;;

    esac

#		test -d /tmp/domainlist-$(hostname -f) || mkdir /tmp/domainlist-$(hostname -f)
#		test -d /tmp/domainlist-$(hostname -f) && (
#		test -d /tmp/domainlist-$(hostname -f)/.git && rm -rf /tmp/domainlist-$(hostname -f)/.git
#		cd /tmp/domainlist-$(hostname -f);
#		git init
#		git remote add origin $HOST_GIT_REPO

#tEST for recent git version , else scratch ( other nodes might have been transferring/updating)
test -d /tmp/domainlist-$(hostname -f)/ && cd /tmp/domainlist-$(hostname -f)/ && git pull || ( rm -rf /tmp/domainlist-$(hostname -f)/;
 echo "::CLONE: "$HOST_GIT_REPO  ; pwd ; git clone $HOST_GIT_REPO /tmp/domainlist-$(hostname -f)/ && md5sum /tmp/vhostconf.domainlist /tmp/domainlist-$(hostname -f)/domainlist ; )
##COMMIT STEP

test -f /tmp/vhostconf.domainlist  && {
    [[ "$DEBUGME" = "true" ]] && echo $(date +%F-%T) updating domainlist
    [[ "$DEBUGME" = "true" ]] && cat /tmp/vhostconf.domainlist
    cat /tmp/vhostconf.domainlist > /tmp/domainlist-$(hostname -f)/domainlist
    cd /tmp/domainlist-$(hostname -f) && git add domainlist && git commit -am $(hostname -f)"domain list "$(date -u +%Y-%m-%d-%H.%M) && git config user.name domainlist@$(hostname -f) && git push -f origin master ; } ;

echo -n ; } ;

case "$1" in
    host )
    #echo "server side";
    _host_extract ;;
    client )
    #echo "client side";
    _websrv_health_client ;;
esac

rm $lockfile
